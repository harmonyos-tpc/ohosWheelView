// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.weigan.loopview;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

// Referenced classes of package com.qingchifan.view:
//            LoopView

final class MessageHandler extends EventHandler {
    public static final int WHAT_INVALIDATE_LOOP_VIEW = 1000;
    public static final int WHAT_SMOOTH_SCROLL = 2000;
    public static final int WHAT_SMOOTH_SCROLL_INERTIA = 2001;
    public static final int WHAT_ITEM_SELECTED = 3000;

    final LoopView loopview;

    MessageHandler(EventRunner eventRunner, LoopView loopview) {
        super(eventRunner);
        this.loopview = loopview;
    }

    @Override
    protected void processEvent(InnerEvent event) {
        switch (event.eventId) {
            case WHAT_INVALIDATE_LOOP_VIEW:
                loopview.invalidate();
                break;

            case WHAT_SMOOTH_SCROLL:
                removeEvent(WHAT_SMOOTH_SCROLL_INERTIA);
                loopview.smoothScroll(LoopView.ACTION.FLING);
                break;

            case WHAT_ITEM_SELECTED:
                loopview.onItemSelected();
                break;
        }
    }

}
