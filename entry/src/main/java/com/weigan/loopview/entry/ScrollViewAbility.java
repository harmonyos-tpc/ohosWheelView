package com.weigan.loopview.entry;

import com.weigan.loopview.LoopView;
import com.weigan.loopview.OnItemSelectedListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;

public class ScrollViewAbility extends Ability {
    private LoopView loopView;
    private Text itemText;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_scroll_view);
        loopView = (LoopView) findComponentById(ResourceTable.Id_loopView);
        itemText = (Text) findComponentById(ResourceTable.Id_item_index);
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            list.add("item " + i);
        }
        // 滚动监听
        loopView.setListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                itemText.setText("item " + index);
            }
        });
        // 设置原始数据
        loopView.setItems(list);

        findComponentById(ResourceTable.Id_button).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                loopView.setCurrentPosition(0);
            }
        });
    }
}
