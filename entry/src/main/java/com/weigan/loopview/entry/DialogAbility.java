package com.weigan.loopview.entry;

import com.weigan.loopview.LoopView;
import com.weigan.loopview.OnItemSelectedListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;

public class DialogAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_dialog);
        findComponentById(ResourceTable.Id_button).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                showDialog();
            }
        });
    }

    private void showDialog() {
        CommonDialog commonDialog = new CommonDialog(this);
        Component dialogComponent = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_dialog_view, null, false);
        final LoopView loopView = (LoopView) dialogComponent.findComponentById(ResourceTable.Id_loopView);
        final Text itemIndex = (Text) dialogComponent.findComponentById(ResourceTable.Id_item_index);
        commonDialog.setContentCustomComponent(dialogComponent);
        commonDialog.setTransparent(true);
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            list.add("item " + i);
        }
        loopView.setItems(list);
        // 滚动监听
        loopView.setListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                itemIndex.setText("item " + index);
            }
        });
        commonDialog.setDestroyedListener(new CommonDialog.DestroyedListener() {
            @Override
            public void onDestroy() {
                ToastDialog toastDialog = new ToastDialog(getContext());
                int selectedItem = loopView.getSelectedItem();
                toastDialog.setText("current item  " + selectedItem);
                toastDialog.setDuration(500);
                toastDialog.show();
            }
        });
        commonDialog.show();
    }
}
