package com.weigan.loopview.entry;

import com.weigan.loopview.LoopView;
import com.weigan.loopview.OnItemScrollListener;
import com.weigan.loopview.OnItemSelectedListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

import java.util.ArrayList;

public class MainAbility extends Ability {
    private LoopView loopView;
    private Text itemText;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        loopView = (LoopView) findComponentById(ResourceTable.Id_loopView);
        itemText = (Text) findComponentById(ResourceTable.Id_item_index);
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 60; i++) {
            list.add("item gp" + i);
        }
        //设置是否循环播放
//        loopView.setNotLoop();
        //滚动监听
        loopView.setListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                itemText.setText("item " + index);
            }
        });
        loopView.setOnItemScrollListener(new OnItemScrollListener() {
            @Override
            public void onItemScrollStateChanged(LoopView loopView, int currentPassItem, int oldScrollState, int scrollState, int totalScrollY) {
            }

            @Override
            public void onItemScrolling(LoopView loopView, int currentPassItem, int scrollState, int totalScrollY) {
            }
        });
        //设置原始数据
        loopView.setItems(list);

        //设置初始位置
        loopView.setInitPosition(4);

        findComponentById(ResourceTable.Id_button).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                loopView.setCurrentPosition(6);
            }
        });

        findComponentById(ResourceTable.Id_button2).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent2 = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(ScrollViewAbility.class).build();
                intent2.setOperation(operation);
                startAbility(intent2);
            }
        });
        findComponentById(ResourceTable.Id_button3).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent2 = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(DialogAbility.class).build();
                intent2.setOperation(operation);
                startAbility(intent2);
            }
        });
    }

}
